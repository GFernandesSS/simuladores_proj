require 'rails_helper'

RSpec.describe "simulators/new", type: :view do
  before(:each) do
    assign(:simulator, Simulator.new(
      :name => "MyString",
      :category_id => 1,
      :url => "MyString"
    ))
  end

  it "renders new simulator form" do
    render

    assert_select "form[action=?][method=?]", simulators_path, "post" do

      assert_select "input#simulator_name[name=?]", "simulator[name]"

      assert_select "input#simulator_category_id[name=?]", "simulator[category_id]"

      assert_select "input#simulator_url[name=?]", "simulator[url]"
    end
  end
end
