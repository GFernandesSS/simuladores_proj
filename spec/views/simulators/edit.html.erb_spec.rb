require 'rails_helper'

RSpec.describe "simulators/edit", type: :view do
  before(:each) do
    @simulator = assign(:simulator, Simulator.create!(
      :name => "MyString",
      :category_id => 1,
      :url => "MyString"
    ))
  end

  it "renders the edit simulator form" do
    render

    assert_select "form[action=?][method=?]", simulator_path(@simulator), "post" do

      assert_select "input#simulator_name[name=?]", "simulator[name]"

      assert_select "input#simulator_category_id[name=?]", "simulator[category_id]"

      assert_select "input#simulator_url[name=?]", "simulator[url]"
    end
  end
end
