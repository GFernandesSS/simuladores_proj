require 'rails_helper'

RSpec.describe "simulators/show", type: :view do
  before(:each) do
    @simulator = assign(:simulator, Simulator.create!(
      :name => "Name",
      :category_id => 1,
      :url => "Url"
    ))
  end

  it "renders attributes in <p>" do
    render
    expect(rendered).to match(/Name/)
    expect(rendered).to match(/1/)
    expect(rendered).to match(/Url/)
  end
end
