require 'rails_helper'

RSpec.describe "simulators/index", type: :view do
  before(:each) do
    assign(:simulators, [
      Simulator.create!(
        :name => "Name",
        :category_id => 1,
        :url => "Url"
      ),
      Simulator.create!(
        :name => "Name",
        :category_id => 1,
        :url => "Url"
      )
    ])
  end

  it "renders a list of simulators" do
    render
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => 1.to_s, :count => 2
    assert_select "tr>td", :text => "Url".to_s, :count => 2
  end
end
