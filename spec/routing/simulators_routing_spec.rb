require "rails_helper"

RSpec.describe SimulatorsController, type: :routing do
  describe "routing" do

    it "routes to #index" do
      expect(:get => "/simulators").to route_to("simulators#index")
    end

    it "routes to #new" do
      expect(:get => "/simulators/new").to route_to("simulators#new")
    end

    it "routes to #show" do
      expect(:get => "/simulators/1").to route_to("simulators#show", :id => "1")
    end

    it "routes to #edit" do
      expect(:get => "/simulators/1/edit").to route_to("simulators#edit", :id => "1")
    end

    it "routes to #create" do
      expect(:post => "/simulators").to route_to("simulators#create")
    end

    it "routes to #update via PUT" do
      expect(:put => "/simulators/1").to route_to("simulators#update", :id => "1")
    end

    it "routes to #update via PATCH" do
      expect(:patch => "/simulators/1").to route_to("simulators#update", :id => "1")
    end

    it "routes to #destroy" do
      expect(:delete => "/simulators/1").to route_to("simulators#destroy", :id => "1")
    end

  end
end
