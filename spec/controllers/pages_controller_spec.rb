require 'rails_helper'

RSpec.describe PagesController, type: :controller do

  describe "GET #duvidas" do
    it "returns http success" do
      get :duvidas
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #sites" do
    it "returns http success" do
      get :sites
      expect(response).to have_http_status(:success)
    end
  end

  describe "GET #sobre" do
    it "returns http success" do
      get :sobre
      expect(response).to have_http_status(:success)
    end
  end

end
