json.array!(@simulators) do |simulator|
  json.extract! simulator, :id, :name, :category_id, :url
  json.url simulator_url(simulator, format: :json)
end
