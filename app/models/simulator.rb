class Simulator < ActiveRecord::Base
	belongs_to :category
	belongs_to :user
	has_attached_file :icon, styles: { medium: "300x300>", thumb: "200x100>" }, default_url: "/images/:style/missing.png"
  	validates_attachment_content_type :icon, content_type: /\Aimage\/.*\Z/
  	acts_as_votable
  	
end