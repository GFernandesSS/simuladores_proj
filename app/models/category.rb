class Category < ActiveRecord::Base
	belongs_to :parent, :class_name => "Category", :foreign_key => "category_id"
	has_many :categories  # TODO adcionar index
	has_many :simulators
	validates :name, presence: true, uniqueness: true
end
