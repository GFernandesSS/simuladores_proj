class SimulatorsController < ApplicationController
  before_action :set_simulator, only: [:show, :edit, :update, :destroy]

  # GET /simulators
  # GET /simulators.json
  def index
    @simulators = Simulator.all
  end

  # GET /simulators/1
  # GET /simulators/1.json
  def show
    @simulator = Simulator.find (params[:id])
  end

  # GET /simulators/new
  def new
    @simulator = Simulator.new
  end

  # GET /simulators/1/edit
  def edit
  end

  # POST /simulators
  # POST /simulators.json
  def create
    @simulator = Simulator.new(simulator_params

    respond_to do |format|
      if @simulator.save
        format.html { redirect_to @simulator, notice: 'Simulator was successfully created.' }
        format.json { render :show, status: :created, location: @simulator }
      else
        format.html { render :new }
        format.json { render json: @simulator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /simulators/1
  # PATCH/PUT /simulators/1.json
  def update
    respond_to do |format|
      if @simulator.update(simulator_params)
        format.html { redirect_to @simulator, notice: 'Simulator was successfully updated.' }
        format.json { render :show, status: :ok, location: @simulator }
      else
        format.html { render :edit }
        format.json { render json: @simulator.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /simulators/1
  # DELETE /simulators/1.json
  def destroy
    @simulator.destroy
    respond_to do |format|
      format.html { redirect_to simulators_url, notice: 'Simulator was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_simulator
      @simulator = Simulator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def simulator_params
      params.require(:simulator).permit(:name, :category_id, :url)
    end
end
