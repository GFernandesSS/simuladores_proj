class SimulatorsController < ApplicationController
  before_action :set_simulator, only: [:show, :edit, :update, :destroy, :like, :dislike]
  
  before_action :authenticate_user!, only: [:new, :edit, :create, :update, :destroy]
  
   before_action :only_mine, only: [:edit, :update, :destroy]

  # GET /simulators
  # GET /simulators.json
  def index
    @simulators = Simulator.all
    @simulators = Simulator.order("name").page(params[:page]).per(6)

    # @simulators = Simulator.order :category_id

    if params[:search].present?
      @simulators = Simulator.where('lower(simulators.name) LIKE ?', "%#{params[:search]}%").order :category_id
      
      categories = Category.where('lower(categories.name) LIKE ?', "%#{params[:search]}%")
      category_simulators = categories.map {|category| category.simulators.to_a}.flatten
      @simulators.push(*category_simulators)
    end

      #@simulators = @simulators.uniq
      # @simulators = Simulator.search(params[:id])
  end

  # GET /simulators/1
  # GET /simulators/1.json
  def show
  end

  # GET /simulators/new
  def new
    @simulator = Simulator.new
  end

  # GET /simulators/1/edit
  def edit
  end

  # POST /simulators
  # POST /simulators.json
  def create
    # @simulator = Simulator.new(simulator_params)
    @simulator = Simulator.create(simulator_params)
    @simulator.user = current_user

    respond_to do |format|
      if @simulator.save
        format.html { redirect_to @simulator, notice: 'Simulador criado com êxito.' }
        format.json { render :show, status: :created, location: @simulator }
      else
        format.html { render :new }
        format.json { render json: @simulator.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /simulators/1
  # PATCH/PUT /simulators/1.json
  def update
    respond_to do |format|
      if @simulator.update(simulator_params) 
        format.html { redirect_to @simulator, notice: 'Simulador atualizado com êxito.' }
        format.json { render :show, status: :ok, location: @simulator }
      else
        format.html { render :edit }
        format.json { render json: @simulator.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /simulators/1
  # DELETE /simulators/1.json
  def destroy
    @simulator.destroy
    respond_to do |format|
      format.html { redirect_to simulators_url, notice: 'Simulador apagado com êxito.' }
      format.json { head :no_content }
    end
  end

  def upvote 
    @simulator = Simulator.find(params[:id])
    @simulator.upvote_by current_user
    redirect_to :back
  end  

  def downvote
    @simulator = Simulator.find(params[:id])
    @simulator.downvote_by current_user
    redirect_to :back
  end
  
  def like
    @simulator.liked_by current_user

    respond_to do |format|
      if user_signed_in?
        format.html { redirect_to simulators_url, notice: 'Simulador avaliado com êxito!' }
        format.json { head :no_content }
      else
        format.html { redirect_to simulators_url, notice: 'Login necessário.' }
      end
    end
  end
  
  def dislike
    @simulator.disliked_by current_user

    respond_to do |format|
      if user_signed_in?
        format.html { redirect_to simulators_url, notice: 'Simulador avaliado com êxito!' }
        format.json { head :no_content }
      else
        format.html { redirect_to simulators_url, notice: 'Login necessário.' }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_simulator
      @simulator = Simulator.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def simulator_params
      params.require(:simulator).permit(:name, :category_id, :description, :url, :icon)
    end
    
    def only_mine
      if @simulator.user != current_user
        head(403)
      end
    end
end