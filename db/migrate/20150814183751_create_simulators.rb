

class CreateSimulators < ActiveRecord::Migration
  def change
    create_table :simulators do |t|
      t.string :name
      t.integer :category_id
      t.string :url

      t.timestamps null: false
    end
  end
end
