class AddDescriptionToSimulators < ActiveRecord::Migration
  def change
    add_column :simulators, :description, :string
  end
end
