class AddUserReftToSimulators < ActiveRecord::Migration
  def change
    add_reference :simulators, :user, index: true, foreign_key: true
  end
end
