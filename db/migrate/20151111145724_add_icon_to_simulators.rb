class AddIconToSimulators < ActiveRecord::Migration
  def up
    add_attachment :simulators, :icon
  end

  def down
    remove_attachment :simulators, :icon
  end
end
